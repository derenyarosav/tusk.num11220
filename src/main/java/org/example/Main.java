package org.example;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        Costumer costumer = new Costumer("Alice");
        Costumer costumer2 = new Costumer("Bob");
        Costumer costumer3 = new Costumer("Charlie");

        List<Costumer> costumerList = new ArrayList<>();
        costumerList.add(costumer);
        costumerList.add(costumer2);
        costumerList.add(costumer3);


        List<Action> actionList = new ArrayList<>();
        List<Action> targetActionList = new ArrayList<>();
        ChangePriceThread changePriceThread = new ChangePriceThread(actionList);
        ExchangeThread exchangeThread = new ExchangeThread(targetActionList, costumerList);
        changePriceThread.start();

        for (int i = 0; i <= 20; i++) {
            int h = 3 * 141 / 100;
            int rand = new Random().nextInt(-h, h + 1);
            int price = rand + 141;
            Action action = new Action("AAPL", 100, price);
            System.out.println("Ціна акцій компанії AAPL змінилась. Поточна вартість: " + price);

            int h1 = 3 * 387 / 100;
            int rand1 = new Random().nextInt(-h1, h1 + 1);
            int price1 = rand1 + 387;
            Action action1 = new Action("COKE", 1000, price1);
            System.out.println("Ціна акцій компанії COKE змінилась. Поточна вартість: " + price1);

            int h2 = 3 * 387 / 100;
            int rand2 = new Random().nextInt(-h2, h2 + 1);
            int price2 = rand2 + 387;
            Action action2 = new Action("IBM", 200, price2);
            System.out.println("Ціна акцій компанії IBM змінилась. Поточна вартість: " + price2);

            actionList.add(action);
            actionList.add(action1);
            actionList.add(action2);

            Thread.sleep(3000);
        }


        exchangeThread.start();

        Action actionAlice = new Action("AAPL", 10, 100);
        Action actionAlice2 = new Action("COKE", 20, 390);
        Action actionBob = new Action("AAPL", 10, 140);
        Action actionBob2 = new Action("IBM", 20, 135);
        Action actionCharlie = new Action("COKE", 300, 370);

        targetActionList.add(actionAlice);
        targetActionList.add(actionAlice2);
        targetActionList.add(actionBob);
        targetActionList.add(actionBob2);
        targetActionList.add(actionCharlie);

        for (int i = 0; i <= 100; i++) {
            if (actionAlice.getPrice() >= actionList.get(0).getPrice()) {
                System.out.println("Спроба купівлі акції AAPL для Alice успішна. Куплено 10 акцій.");
            } else {
                System.out.println("Спроба купівлі акції AAPL для Alice не успішна.");
            }
            if (actionAlice2.getPrice() >= actionList.get(1).getPrice()) {
                System.out.println("Спроба купівлі акції COKE для Alice успішна. Куплено 20 акцій.");
            } else {
                System.out.println("Спроба купівлі акції COKE для Alice не успішна.");
            }
            if (actionBob.getPrice() >= actionList.get(0).getPrice()) {
                System.out.println("Спроба купівлі акції AAPL для Bob успішна. Куплено 10 акцій.");
            } else {
                System.out.println("Спроба купівлі акції AAPL для Bob не успішна.");
            }
            if (actionBob2.getPrice() >= actionList.get(2).getPrice()) {
                System.out.println("Спроба купівлі акції IBM для Bob успішна. Куплено 20 акцій.");
            } else {
                System.out.println("Спроба купівлі акції IBM для Bob не успішна.");
            }
            if (actionCharlie.getPrice() >= actionList.get(1).getPrice()) {
                System.out.println("Спроба купівлі акції COKE для Charlie успішна. Куплено 300 акцій.");
            } else {
                System.out.println("Спроба купівлі акції COKE для Charlie не успішна.");
            }
            Thread.sleep(5000);
        }
    }
}
