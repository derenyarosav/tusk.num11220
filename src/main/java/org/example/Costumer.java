package org.example;



import java.util.ArrayList;
import java.util.List;

public class Costumer {
     private String name ;

     List<Action> target = new ArrayList<>();

     public Costumer( String name ){
         this.name = name;
     }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Costumer{" +
                "name='" + name + '\'' +
                '}';
    }
}
